
//script for displaying array in JS (Method 1) starts 
var arraytest = new Array();
arraytest[0] = "Apple";
arraytest[1] = "Banana";
arraytest[2] = "Orange";
arraytest[3] = "Pineapple";
arraytest[4] = "Kiwi";
document.getElementById("company").innerHTML = arraytest;
//script for displaying array in JS (Method 1) ends 


//script for displaying array in JS (Method 2) starts 
var arraytest2 = ["Kiwi", "Pineapple", "Orange", "Banana", "Apple"];
document.getElementById("company2").innerHTML = arraytest2;
//script for displaying array in JS (Method 2) ends 


//on click functions (Text Changer) with method 1 & 2 JS starts 
document.getElementById("clc").onclick = function () {
    document.getElementById("company3").innerHTML = arraytest2;
};
//on click functions (Text Changer) with method 1 & 2 JS starts ends



//on click functions (Text Appender) with method 1 & 2 JS starts 
document.getElementById("clc2").onclick = function () {
    document.getElementById("company4").innerHTML =
            document.getElementById("company4").innerHTML + arraytest2;
};
//on click functions (Text Appender) with method 1 & 2 JS starts ends



//on click functions (Styling) with method 1 & 2 JS starts 
document.getElementById("company5").onclick = function () {
    document.getElementById("company5").style.backgroundColor = "yellow";
};
//on click functions (Styling) with method 1 & 2 JS starts ends



//on click functions (Multiple Styling) with method 1 & 2 JS starts 
document.getElementById("company6").onclick = function () {
    document.getElementById("company6").style.backgroundColor = "red";
    document.getElementById("company6").style.border = "8px solid black";
};
//on click functions (Multiple Styling) with method 1 & 2 JS starts ends



//on click functions (Text Changer) with method 1 & 2 JS starts 
document.getElementById("company8").onclick = function () {
    document.getElementById("company9").innerHTML = document.getElementById("company7").value;
};
//on click functions (Text Changer) with method 1 & 2 JS starts ends


//calculating length
document.getElementById("company10").innerHTML = arraytest.length;

//joining
document.getElementById("company11").innerHTML = arraytest.join('*');


//popping items starts
document.getElementById("company13").innerHTML = arraytest;
document.getElementById("company12").onclick = function () {
    arraytest.pop();
    document.getElementById("company13").innerHTML = arraytest;
};
//popping items ends


//pushing items starts
document.getElementById("company15").innerHTML = arraytest;
document.getElementById("company14").onclick = function () {
    arraytest.push("Peach");
    document.getElementById("company15").innerHTML = arraytest;
};
//pushing items ends

//SHifting items starts
document.getElementById("company17").innerHTML = arraytest;
document.getElementById("company16").onclick = function () {
    arraytest.shift();
    document.getElementById("company17").innerHTML = arraytest;
};
//SHifting items ends


//unSHifting items ends
document.getElementById("company19").innerHTML = arraytest;
document.getElementById("company18").onclick = function () {
    arraytest.unshift('Peach');
    document.getElementById("company19").innerHTML = arraytest;
};
//unSHifting items ends


//splicing items adds starts
document.getElementById("company21").innerHTML = arraytest;
document.getElementById("company20").onclick = function () {
    arraytest.splice(1, 0, "Peach");
    document.getElementById("company21").innerHTML = arraytest;
};
//splicing items adds ends


//splicing items remove starts
document.getElementById("company23").innerHTML = arraytest;
document.getElementById("company22").onclick = function () {
    arraytest.splice(3, 1);
    document.getElementById("company23").innerHTML = arraytest;
};
//splicing items remove ends


//sorting items sarts
document.getElementById("company25").innerHTML = arraytest;
document.getElementById("company24").onclick = function () {
    arraytest.sort();
    document.getElementById("company25").innerHTML = arraytest;
};
//sorting items ends


//reverse items starts
document.getElementById("company27").innerHTML = arraytest;
document.getElementById("company26").onclick = function () {
    arraytest.reverse();
    document.getElementById("company27").innerHTML = arraytest;
};
//reverse items ends


//concatenate items starts
document.getElementById("company29").innerHTML = arraytest;
document.getElementById("company30").innerHTML = arraytest2;
document.getElementById("company28").onclick = function () {
    var arraytest3 = arraytest.concat(arraytest2);
    document.getElementById("company31").innerHTML = arraytest3;
};
//concatenate items ends