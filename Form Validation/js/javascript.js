/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function()
{
    $("#phone").keydown(function(e)
    {
//code to format phone no. to contain "-"
        if (e.keyCode !== 8) {
             if ($(this).val().length === 0) {
                $(this).val($(this).val() + "(");
            }
            else if ($(this).val().length === 4) {
                $(this).val($(this).val() + ")-");
            } else if ($(this).val().length === 9) {
                $(this).val($(this).val() + "-");
            }
        }
    });

//prevents future date to be selected for date of birth
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    month = month + 1;
    if (month < 10)
    {
        month = '0' + month;
    }
    var year = date.getFullYear();
    var currentdate = (year + "-" + month + "-" + day);
    $("#dob").attr('max', currentdate);

//sets focus color for all textboxes
    $("input").focus(function()
    {
        $(this).css({"border": "1.5px solid #e2aee3", "outline": "none", "box-shadow": "0 0 15px #ed9eec","height":"19px"});
    });

//  sets blur color for all textboxes
    $("input").blur(function()
    {
        $(this).css({"border": "1.5px solid #e1e1e1", "box-shadow": "0 0 5px #959595 inset", "border-radius": "5px", "height":"20px"});
    });

//  functions to be performed on clicking the submit button 
    $("#button-submit").click(function()
    {
        event.preventDefault();


//    code to restrict null value and numbers in first name
        valid = true;
        if ($("#f-name").val() === "" || null)
        {
            $("#err_f-name_empty").css("color", "red");
            $("#err_f-name_empty").html("Please enter your first name");
            valid = false;
        }
        else
        {
            var reg = /^[a-zA-Z]*$/;
            if (!(reg.test($("#f-name").val())))
            {
                $("#err_f-name_empty").hide();
                $("#err_f-name_wrong").css("color", "red");
                $("#err_f-name_wrong").html("First name can only contain alphabets");
                valid = false;
            }
            else
            {$("#err_f-name_empty").hide();
            $("#err_f-name_wrong").hide();}
        }

//   code to restrict null value and numbers in last name
        valid = true;
        if ($("#l-name").val() === "" || null)
        {
            $("#err_l-name_empty").css("color", "red");
            $("#err_l-name_empty").html("Please enter your last name");
            valid = false;
        }
        else
        {
            var reg = /^[a-zA-Z]*$/;
            if (!(reg.test($("#l-name").val())))
            {
                $("#err_l-name_empty").hide();
                $("#err_l-name_wrong").css("color", "red");
                $("#err_l-name_wrong").html("Last name can only contain alphabets");
                valid = false;
            }
            else
            { $("#err_l-name_empty").hide();
            $("#err_l-name_wrong").hide();}
        }

//code to check that one radio button of gender is selected
        if (!$('input[name="gender"]:checked').val())
        {
            $("#err_gender_empty").css("color", "red");
            $("#err_gender_empty").html("Please select gender");
            valid = false;
        }
        else
            { $("#err_gender_empty").hide();}

//code to check date of birth should not be null
        if ($("#dob").val() === "" || null)
        {
            $("#err_dob_empty").css("color", "red");
            $("#err_dob_empty").html("Please select Date of Birth");
            valid = false;
        }
        else
            { $("#err_dob_empty").hide();}


//code to check phone no. should not be empty 
        if ($("#phone").val() === "" || null)
        {
            $("#err_phone_empty").css("color", "red");
            $("#err_phone_empty").html("Please enter phone no.");
            valid = false;
        }

//code to restrict alphabets and special characters in phone numbers
        else
        {
            var tel = /^[0-9-@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]*$/;
            if (!tel.test($("#phone").val()))
            {
                $("#err_phone_empty").hide();
                $("#err_phone_wrong").css("color", "red");
                $("#err_phone_wrong").html("Phone no. can only contain numbers");
                valid = false;
            }
            else
            {  $("#err_phone_empty").hide();
            $("#err_phone_wrong").hide();}

        }

//code to restrict null or invalid email
        if ($("#email").val() === "" || null)
        {
            $("#err_email_empty").css("color", "red");
            $("#err_email_empty").html("Please enter email");
            valid = false;
        }
        else
        {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!(reg.test($("#email").val())))
            {
                $("#err_email_empty").hide();
                $("#err_email_invalid").css("color", "red");
                $("#err_email_invalid").html("E-mail invalid");
                valid = false;
            }
            else
            { $("#err_email_empty").hide();
            $("#err_email_invalid").hide();}
        }

// code to restrict empty password and no. of characters in password
        if ($("#pass").val() === "" || null)
        {
            $("#err_pass_empty").css("color", "red");
            $("#err_pass_empty").html("Please enter password");
            valid = false;
        }
        else {
            if ($("#pass").val().length <= 5)
            {
                $("#err_pass_empty").hide();
                $("#err_pass_length").css("color", "red");
                $("#err_pass_length").html("Minimum 6 characters");
                valid = false;
            }
            else
            { $("#err_pass_empty").hide();
            $("#err_pass_length").hide();}
        }

//  code to restrict emty confirm password field and to match password and confirm password fields      
        if ($("#cpass").val() === "" || null)
        {
            $("#err_cpass_empty").css("color", "red");
            $("#err_cpass_empty").html("Please confirm password");
            valid = false;
        }
        else
        {
            if ($("#pass").val() !== $("#cpass").val())
            {
                $("#err_cpass_empty").hide();
                $("#err_cpass_match").css("color", "red");
                $("#err_cpass_match").html("Password does not match");
                valid = false;
            }
            else
            { $("#err_cpass_empty").hide();
            $("#err_cpass_match").hide();}
        }

//code to check atleast one checkbox is selected
        if ($('input[type=checkbox]:checked').length === 0)
        {
            $("#err_hobby_empty").css("color", "red");
            $("#err_hobby_empty").html("Please select hobby");
            valid = false;
        }
        else
            { $("#err_hobby_empty").hide();}

// code to display submit seccessful message and hide error msgs
        if (valid === true)
        {
            $("#submit_success").html("Form submitted successfully");
            $("#err_hobby_empty").hide();
            $("#err_f-name_empty").hide();
            $("#err_f-name_wrong").hide();
            $("#err_l-name_empty").hide();
            $("#err_l-name_wrong").hide();
            $("#err_gender_empty").hide();
            $("#err_dob_empty").hide();
            $("#err_phone_empty").hide();
            $("#err_phone_wrong").hide();
            $("#err_phone_length").hide();
            $("#err_email_empty").hide();
            $("#err_email_invalid").hide();
            $("#err_pass_empty").hide();
            $("#err_pass_length").hide();
            $("#err_cpass_empty").hide();
            $("#err_cpass_match").hide();
            $("#err_hobby_empty").hide();
        }
    });
});

